
AUTOMATION_VERSION := tags/release/1.2
AUTOMATION_GIT_PATH := accidentallythecable-code/utils/automation-tools
AUTOMATION_GIT_HOST := gitlab.com

PROJECT_OWNER := AccidentallyTheCable
PROJECT_EMAIL := cableninja@cableninja.net
PROJECT_FIRST_YEAR := 2023
PROJECT_LICENSE := GPLv2+
PROJECT_NAME := pycryptsetup
PROJECT_DESCRIPTION := Python bindings for cryptsetup
PROJECT_VERSION := 1.0.0

## Enable Feature 'Python'
BUILD_PYTHON := 1
## Enable Feature 'Shell'
BUILD_SHELL := 0
## Enable python `dist` Phase for Projects destined for PYPI
PYTHON_PYPI_PROJECT := 1
## Additional Flags for pylint. EX --ignore-paths=mypath
PYLINT_EXTRA_FLAGS := 
PYTHON_PROJECT_NAME := ${PROJECT_NAME}
PYTHON_PROJECT_VERSION := ${PROJECT_VERSION}

### Any Further Project-specific make targets can go here		
project_clean:  ## Clean
	rm -rf build dist
	rm -rf *.egg-info

project_build_deps:  ## Project Build Deps
	apt -q update
	DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true apt -y install libcryptsetup-dev
