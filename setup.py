from distutils.core import setup, Extension
setup(
    name="pycryptsetup",
    version = '1.0.0',
    description = "Python bindings for cryptsetup",
    long_description_content_type="text/markdown",
    long_description="README.md",
    author = "Martin Sivak, AccidentallyTheCable",
    author_email= "msivak@redhat.com, cableninja@cableninja.net",
    license = 'GPLv2+',
    packages = ["pycryptsetup"],
    ext_modules = [Extension("cryptsetup", ["pycryptsetup/cryptsetup.c"], library_dirs=['.'], libraries=['cryptsetup'])],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Security :: Cryptography",
        "Topic :: Software Development :: Libraries :: Python Modules"
    ]
)
