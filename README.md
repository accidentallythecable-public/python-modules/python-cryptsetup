# Python Cryptsetup Library

C Bindings for Cryptsetup.

This was created as a realization that this did not already exist in this form. Redhat, and through a fork, Android, both have this in place, however there is nothing on pypi for it. This file uses the forked android [pycryptsetup.c](https://android.googlesource.com/platform/external/cryptsetup/+/refs/heads/main/python/pycryptsetup.c?format=TEXT) to create a Python3 library for cryptsetup, available via pypi.

***WARNING:*** This project will conflict with any other installed `python-cryptsetup` package whose package name is `pycryptsetup`. This has not been modified in this project.

[Cryptsetup Man Page](https://man7.org/linux/man-pages/man8/cryptsetup.8.html)

## Dependencies

You will need your OS-Specific `libcryptsetup-dev` (And `cryptsetup`, obviously).
